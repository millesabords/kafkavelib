import json
import time
import urllib.request

from kafka import KafkaProducer

#API_KEY = "XXX" # FIXME Set your own API key here
url = "https://velib-metropole-opendata.smoove.pro/opendata/Velib_Metropole/station_status.json"
producer = KafkaProducer(bootstrap_servers="localhost:9092")
max_stations_to_check = 40

while True:
    response = urllib.request.urlopen(url)
    raw_stations_data = json.loads(response.read().decode())

    #the json file is currently formatted as follows:
    #the loaded data (~root) is a dictionnary that only has one entry whose key is "data" and value is another dictionnary
    #this new dictionnary has only one entry with key "stations and value is the list that contains the info we need
    #hence the hereafter way to access to our stations variable, which is a list:
    stations = raw_stations_data["data"]["stations"]
    print("debug1: dumping stations".format(json.dumps(stations)))
    print("debug2: stations length: {} stations type: {}".format(len(stations), type(stations)))
    #there is currently 1462 stations listed under this access platform, so let's just use a few
    cpt_stations = 0
    for station in stations:
        cpt_stations += 1
        if cpt_stations == max_stations_to_check: break
        producer.send("velib-stations", json.dumps(station).encode())
        print("\tstation information: {}".format(json.dumps(station)))
    print("{} Produced {} station records".format(time.time(), len(stations)))
    time.sleep(10)
