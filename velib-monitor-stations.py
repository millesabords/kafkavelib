import json
from kafka import KafkaConsumer

#stations is our main dictionnary, key will be station_id code, value will be number of available bikes (number of docks supposedly never changes)
stations = {}

###todo: try with groups (careful the way consumer groups are sharing a topic's messages across one partition, or several partitions, which is totally different)
###consumer = KafkaConsumer("velib-stations", bootstrap_servers='localhost:9092', group_id="velib-monitor-stations")
consumer = KafkaConsumer("velib-stations", bootstrap_servers='localhost:9092')

print("debug0: records type: {}".format(type(consumer)))
for message in consumer:
    station_object = json.loads(message.value.decode())
    station_id = station_object["station_id"]
    station_available_bikes_nb = station_object["num_bikes_available"]

    if station_id not in stations:
        stations[station_id] = {}
    else:
        if stations[station_id] != station_available_bikes_nb:
            print("Station number {} has new number of available bikes: {}".format(station_id, station_available_bikes_nb))
    stations[station_id] = station_available_bikes_nb
