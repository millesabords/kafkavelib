Projet pedagogique en vue de comprendre le fonctionnement concret de Apache Kafka, meme sur un petit exemple ne comportant qu'un seul cluster

Adresse du tutoriel utilise:
https://openclassrooms.com/fr/courses/4451251-gerez-des-flux-de-donnees-temps-reel/4451526-creez-votre-premiere-application-avec-kafka
Complement au tutoriel (jcdecaux n'est plus le gestionnaire des velibs, ni de l'api):
https://www.velib-metropole.fr/donnees-open-data-gbfs-du-service-velib-metropole

requis:
- python3
- kafka-python
- curl, wget ou equivalent pour recuperer a la main les json et faire des tests
- kafka

utilisation:
- ne pas hesiter a creer une fenetre d'instance shell pour chaque executable lance
- lancer le gestionnaire de cluster kafka "zookeeper": (suivre la section 1 du tuto en lien, complementer avec la doc officielle a jour de kafka pour les cas ou les commandes ne sont pas operationnelles)
	$ ./zookeeper-server-start.sh ../config/zookeeper.properties
- lancer un broker kafka (le creer si pas encore fait)
	$ ./kafka-server-start.sh ../config/server.properties &
- creer un topic kafka (pas de photo sans appareil photo ahah....hum)
	$ ./kafka-topics.sh --bootstrap-server localhost:9092 --create --topic velib-stations --partitions 1 --replication-factor 1
- lancer un producteur sur le topic
	$ python3 velib-get-stations.py
- lancer un consommateur sur ce topic:
	$ python3 velib-monitor-stations.py

description:
- zookeeper dirige les cluster pour kafka (meme si ici il n'y a pas ce genre de problematiques)
- le server kafka est notre broker
- le script "producteur" va chercher des donnees sur l'api open-bar du service velib pour recup des infos temps reel sous format json, et ensuite donner fournir ces infos sous forme de message au broker
- le script "consumer" va --todo--
									--> on cherche a visualiser les fluctuations du nombre des emplacements libre de chaque station (ou juste 3 ou 4 pour pas saturer l'affichage)

notes:
- la version actuelle de kafka differe quelque peu dans l'utilisation de certains scripts tels qu'utilises dans le tutoriel
 voir la doc officielle a jour pour corriger quelques options dans les lignes de commande (notemment l'option "bootstrap-server"):
 https://kafka.apache.org/documentation/
- on peut remplacer les scripts python de prod/consumer par des scripts basique fournis par Apache kafka si c'est juste pour se faire une idee rapide du workflow de base de kafka. Il suffit, apres avoir lance zookeeper et le server/broker kafka, de lancer les 2 scripts shell suivants dans des fenetres qui leur sont propres:
	$ ./kafka-console-producer.sh --broker-list localhost:9092 --topic velib-stations
	$ ./kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic velib-stations

exemples de commandes:
- lister les topics deja crees sur le serveur:
$ ./kafka-topics.sh --list --bootstrap-server localhost:9092
$ ./kafka-topics.sh --list --describe --bootstrap-server localhost:9092



schemas and data example (use 3 first stations static info from first table and keep their "capacity" and stationCode and/or station_id as keys, then read second table in consumer by retrieving codes and exploiting what is being done about the 35 bikes or so...
station_information extract:

    "data": {
        "stations": [
            {
                "capacity": 35,
                "lat": 48.865983,
                "lon": 2.275725,
                "name": "Benjamin Godard - Victor Hugo",
                "stationCode": "16107",
                "station_id": 213688169
            },
            {
                "capacity": 30,
                "lat": 48.871256519012,
                "lon": 2.4865807592869,
                "name": "Mairie de Rosny-sous-Bois",
                "rental_methods": [
                    "CREDITCARD"
                ],
                "stationCode": "31104",
                "station_id": 653222953
            },
            {
                "capacity": 21,
                "lat": 48.87929591733507,
                "lon": 2.3373600840568547,
                "name": "Toudouze - Clauzel",
                "rental_methods": [
                    "CREDITCARD"
                ],
                "stationCode": "9020",
                "station_id": 36255

station_status extract:
    data": {
        "stations": [
            {
                "is_installed": 1,
                "is_renting": 1,
                "is_returning": 1,
                "last_reported": 1684176158,
                "numBikesAvailable": 4,
                "numDocksAvailable": 30,
                "num_bikes_available": 4,
                "num_bikes_available_types": [
                    {
                        "mechanical": 1
                    },
                    {
                        "ebike": 3
                    }
                ],
                "num_docks_available": 30,
                "stationCode": "16107",
                "station_id": 213688169
            },
            {

